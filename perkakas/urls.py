from django.urls import path
from .views import index, AlatDetailView, AlatCreateView #new
from .views import AlatEditView, AlatDeleteView, AlatToPdf #new

urlpatterns = [
    path('', index, name='home_page'),
    path('peralatan/<int:pk>', AlatDetailView.as_view(), #new
        name = 'alat_detail_view'),
    path('peralatan/add', AlatCreateView.as_view(), name='alat_add'), #new
    path('peralatan/edit/<int:pk>', AlatEditView.as_view(), name='alat_edit'), #new
    path('peralatan/delete/<int:pk>', AlatDeleteView.as_view(), name='alat_delete'), #new
    path('peralatan/print_pdf', AlatToPdf.as_view(), name='alat_to_pdf') #new
]